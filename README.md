[how-i-gained-access-to-tmobiles-national-network-for-free](https://medium.com/@jacobajit/how-i-gained-access-to-tmobiles-national-network-for-free-f9aaf9273dea#.u094tii5a)

# About Glype

Glype® is a web-based proxy script written in PHP which focuses on features,
functionality, and ease of use. Webmasters use Glype to quickly and easily set
up their own proxy sites. Glype helps users to defeat Internet censorship and
be anonymous while web browsing. There have been over 1,005,000 downloads of
Glype since 2007. Thousands of web-based proxy websites are powered by Glype.

If you are looking for a hosted proxy service, try [Proxify](https://proxify.com/).

# About Web Proxies

A web-based proxy script is hosted on a website which
provides a proxy service to users via a web browser. A proxy service downloads
requested web pages, modifies them for compatibility with the proxy, and
forwards them on to the user. Web proxies are commonly used for anonymous
browsing and bypassing censorship and other restrictions.

# Glype Features
- Free for personal use and licensing options are available for
commercial use.
- Source Viewable and webmasters may modify the source code
subject to the terms of the Software License Agreement.
- Plug and Play. Simply
upload, configure and go!
- Admin Control Panel for easy management and
configuration.
- JavaScript Support provides increased compatibility with
websites.
- Skinable. A theme system allows for customization of your proxy.
- Access Controls blacklist users by IP address and websites by domain name.
- Blocked.com® Integration protects the proxy by blocking specificed countries,
filtering companies, malicious traffic, bots and spiders, and more.
- Unique URLs provide greater privacy by expiring URLs in the browser history at the
end of a browsing session.
- Plugins allow for easy installion of site-specific
modifications. Useful for adding new functionality to websites.
- Advanced Options let users change their user-agent and referrer, manage cookies, and
remove JavaScripts and Flash.
- And much more!

# System Requirements
The script requires PHP version 5.0.x - 5.6.x with cURL support.
